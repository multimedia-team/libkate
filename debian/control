Source: libkate
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders: Petter Reinholdtsen <pere@debian.org>,
           Martin Steghöfer <martin@steghoefer.eu>,
           Ralph Giles <giles@thaumas.net>
Build-Depends: debhelper-compat (= 13),
               dh-exec,
               dh-python,
               doxygen,
               liboggz-dev,
               libpng-dev,
               oggz-tools,
               pkgconf,
               python3,
               python3-wxgtk4.0
Standards-Version: 4.7.0
Section: libs
Homepage: https://wiki.xiph.org/index.php/OggKate
Vcs-Git: https://salsa.debian.org/multimedia-team/libkate.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/libkate

Package: libkate-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libkate1 (= ${binary:Version}), ${misc:Depends}
Description: Codec for karaoke and text encapsulation (dev)
 Kate is meant to be used for karaoke alongside audio/video streams (typically
 Vorbis and Theora), movie subtitles, song lyrics, and anything that needs text
 data at arbitrary time intervals.
 .
 libkate provides an API for the encoding and decoding of kate files.
 This package contains the development libraries.

Package: libkate1
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Codec for karaoke and text encapsulation
 Kate is meant to be used for karaoke alongside audio/video streams (typically
 Vorbis and Theora), movie subtitles, song lyrics, and anything that needs text
 data at arbitrary time intervals.
 .
 libkate provides an API for the encoding and decoding of kate files.

Package: liboggkate-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: liboggkate1 (= ${binary:Version}), ${misc:Depends}
Description: Codec for karaoke and text encapsulation for Ogg (dev)
 Kate is meant to be used for karaoke alongside audio/video streams (typically
 Vorbis and Theora), movie subtitles, song lyrics, and anything that needs text
 data at arbitrary time intervals.
 .
 liboggkate provides an API for the encapsulation of kate streams into Ogg.
 This package contains the development libraries.

Package: liboggkate1
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Codec for karaoke and text encapsulation for Ogg
 Kate is meant to be used for karaoke alongside audio/video streams (typically
 Vorbis and Theora), movie subtitles, song lyrics, and anything that needs text
 data at arbitrary time intervals.
 .
 liboggkate provides an API for the encapsulation of kate streams into Ogg.

Package: libkate-tools
Section: utils
Architecture: any
Multi-Arch: foreign
Depends: oggz-tools, ${misc:Depends}, ${python3:Depends}, ${shlibs:Depends}
Description: Utilities for mangling kate Ogg files
 Kate is meant to be used for karaoke alongside audio/video streams (typically
 Vorbis and Theora), movie subtitles, song lyrics, and anything that needs text
 data at arbitrary time intervals.
 .
 This package contains some utilities useful for debugging and tweaking
 Kate files, using libkate.
